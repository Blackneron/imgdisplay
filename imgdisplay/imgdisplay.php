<?php

// =====================================================================
//                 C O N F I G U R A T I O N - B E G I N
// =====================================================================

// Please specify the title of the page.
$page_title = "Nice Animals";

// Please specify the subtitle of the page.
$page_subtitle = "Click on the titles to open the content.";

// Please specify the company name.
$company_name = "PB-Soft";

// Please specify the link to the website.
$website_url = "https://pb-soft.com";

// Please specify the maximum width of the accordeon (in pixel).
$max_width = 1024;


// =====================================================================
//                   C O N F I G U R A T I O N - E N D
// =====================================================================

// Initialize the images array.
$images = [];

// Initialize the image counter.
$counter = 0;

// Loop through all the images - Begin.
foreach (glob("img/{*.[jJ][pP][gG],*.[jJ][pP][eE][gG],*.[pP][nN][gG],*.[gG][iI][fF]}", GLOB_BRACE) as $image) {

  // Increase the image counter.
  $counter++;

  // Save the image path.
  $images[$counter]['image_path'] = $image;

  // Save the filename without extension.
  $images[$counter]['file_name'] = pathinfo($image, PATHINFO_FILENAME);

  // Save the file extension
  $images[$counter]['file_extension'] = pathinfo($image, PATHINFO_EXTENSION);

  // Save the image title.
  $images[$counter]['image_title'] = str_replace("_", " ", $images[$counter]['file_name']);

} // Loop through all the images - End.

// Sort the images array by the filename.
usort($images, function($a, $b) {
    return strnatcmp($a['file_name'], $b['file_name']);
});

// Specify the document type.
echo "<!DOCTYPE html>\n";

// Display the HTML section - Begin.
echo "<html lang=\"en\">\n";

// Display the page header - Begin.
echo "<head>\n";

// Specify the content type.
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n";

// Specify the viewport.
echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n";

// Display the page title.
echo "<title>".$page_title."</title>\n";

// Display the CSS styles - Begin.
echo "<style>\n";

?>

body {
  background: #f0f8ff;
  font-family: arial, sans-serif;
}

.container {
  margin: 0 auto;
  max-width: <?php echo $max_width; ?>px;
  width: 100%;
}

.title {
  background: #0c4677;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  color: white;
  margin-top: 40px;
  padding: 20px;
}

.title > h1 {
  font-size: 2.5em;
  padding: 0 20px;
}

.title > p {
  font-size: 1em;
  padding: 0 20px;
}

.img-title {
  background-color: #c4ddf3;
  border-bottom: 1px solid #a3c2de;
  border-left: 1px solid #a3c2de;
  border-right: 1px solid #a3c2de;
  border-top: none;
  color: #000066;
  cursor: pointer;
  font-size: 1.3em;
  outline: none;
  padding: 20px;
  text-align: left;
  width: 100%;
}

.active, .img-title:hover {
  background-color: #ffe19e;
  border-bottom: 1px solid #ffbc00;
  border-left: 1px solid #ffbc00;
  border-right: 1px solid #ffbc00;
}

.panel {
  background-color: #ffefcb;
  max-height: 0;
  overflow: hidden;
  text-align: center;
  transition: max-height 0.6s ease-out;
}

img {
  display: block;
  margin: 0 auto;
  max-width: 100%;
}

.footer {
  background: #0c4677;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  color: white;
  font-size: 1em;
  margin-bottom: 40px;
  padding: 20px;
  text-align: center;
}

.footer > a {
  color: #ffbc00;
  text-decoration: none;
}

.footer > a:hover {
  color: yellow;
}

@media screen and (max-width: 767px) {

  .title {
    margin-top: 8px;
  }

  .title > h1 {
    font-size: 2em;
  }

  .img-title {
    font-size: 1.5em;
  }

  .footer {
    margin-bottom: 8px;
  }
}

<?php

// Display the CSS styles - End.
echo "</style>\n";

// Display the page header - End.
echo "</head>\n";

// Display the HTML body - Begin.
echo "<body>\n";

// Container - Begin.
echo "<div class=\"container\">\n";

// Display the title box - Begin.
echo "<div class=\"title\">\n";

// Display the title.
echo "<h1>".$page_title."</h1>\n";

// Display the subtitle.
echo "<p>".$page_subtitle."</p>\n";

// Display the title box - End.
echo "</div>\n";

// Initialize the image counter.
$counter = 0;

// Loop through all the images - Begin.
foreach($images as $image) {

  // Increase the image counter.
  $counter++;

  // Display the button with the image title.
  echo "<button class=\"img-title\">".$counter.". ".$image['image_title']."</button>\n";

  // Display the image panel - Begin.
  echo "<div class=\"panel\">\n";

  // Insert the image link - Begin.
  echo "<a href=\"img/".$image['file_name'].".".$image['file_extension']."\" title=\"".$image['image_title']."\" target=\"_blank\">\n";

  // Insert the image.
  echo "<img src=\"".$image['image_path']."\" alt=\"".$image['image_title']."\">\n";

  // Insert the image link - End.
  echo "</a>\n";

  // Display the image panel - End.
  echo "</div>\n";

} // Loop through all the images - End.

// Display the footer box - Begin.
echo "<div class=\"footer\">\n";

// Display the copyright information.
echo "Copyright ".date("Y")." by <a href=\"".$website_url."\" title=\"".$company_name."\" target=\"_blank\">".$company_name."</a>\n";

// Display the footer box - End.
echo "</div>\n";

// Container - End.
echo "</div>\n";

// Insert the JavaScript code - Begin.
echo "<script>\n";

?>

  // Get all the title buttons.
  var buttons = document.getElementsByClassName("img-title");

  // Initialize the counter variable.
  var i;

  // Loop through all the buttons - Begin.
  for (i = 0; i < buttons.length; i++) {

    // Add a click event listener to each button - Begin.
    buttons[i].addEventListener("click", function() {

      // On click, toggle the button active.
      this.classList.toggle("active");

      // Get the panel object.
      var panel = this.nextElementSibling;

      // Check if the panel is open.
      if (panel.style.maxHeight){

        // Close the image panel.
        panel.style.maxHeight = null;

      // The panel is not open.
      } else {

        // Open the image panel.
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    }); // Add a click event listener to each button - End.

  } // Loop through all the buttons - End.

<?php

// Insert the JavaScript code - End.
echo "</script>\n";

// Display the HTML body - End.
echo "</body>\n";

// Display the HTML section - End.
echo "</html>\n";

?>
