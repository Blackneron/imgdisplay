# IMGDisplay Script - README #
---

### Overview ###

The **IMGDisplay** tool is a simple PHP script which lists images in an vertical accordion. The filenames will be the titles where all underscores **_** will be replaced by a space. In the small configuration section in the header of the script the output can be customized.

### Screenshots ###

![IMGDisplay - Accordion closed](development/readme/imgdisplay1.png "IMGDisplay - Accordion closed")

![IMGDisplay - Accordion open](development/readme/imgdisplay2.png "IMGDisplay - Accordion open")

### Setup ###

* Copy the whole directory **imgdisplay** to your webhost.
* The directory contains the **imgdisplay.php** script and the subdirectory **img**.
* There is also a file called **imgdisplay.min.php** which is a minified version.
* Check that the script file permissions are set to **0644**.
* Check that the directory permissions are set to **0755**.
* Resize and optimize your images (reduce file size).
* Upload all the images into the subfolder **img** and replace the existing ones.
* Open your browser and navigate to the page https://<YOUR_DOMAIN>/imgdisplay/imgdisplay.php
* If you want you can also rename the script **imgdisplay.php**.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **IMGDisplay** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
